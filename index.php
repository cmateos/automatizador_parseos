<html>
    <head>
        <title>Correcció XML</title>
        <meta charset="UTF-8">
        <LINK href="page.css" rel="stylesheet" type="text/css">
            </head>
        <body >
            <div class = "top">
                <h1> Correcció XML per Dummies </h1>
            </div>
            <div class= "body">
                <?php
                if(isset($_POST["boto"]) ){
                    require('functions.php');
                    $repetir = 0;
                    $info = [];
                    $info[] = $_FILES['file']['name'][0];
                    $min_short = -1;
                    $min_syl = -1;
                    //print_r($_FILES['file']['name']);
                    for($i = 0; $i < sizeof($_FILES['file']['name']); $i++){
                        $nom_fitxer = $_FILES['file']['name'][$i];
                        $nom_new = $_POST['name'] . ($i + 1) . ".xml";
                        $info[] = $nom_new;
                        setcookie("nou", serialize($info), time()+3600);
                        if  ($_FILES['file']['size'][$i]  ==  0) die("ERROR: El fitxer en qúestió està buit!");
                        if  (!is_uploaded_file($_FILES['file']['tmp_name'][$i])) die("ERROR:  No s'ha pujat el fitxer!");
                        $uploadDir  =  "./";
                        move_uploaded_file($_FILES['file']['tmp_name'][$i],  $uploadDir  .  $_FILES['file']['name'][$i])  or  die("No s'ha pogut copiar el fitxer al servidor per al tractament");
                        echo  "Fitxer pujat correctament!  </br>";
                        echo $nom_fitxer . " sortirà com a " . $nom_new . "</br>";
                        $repetir = correccio($nom_fitxer,$nom_new);
                        if($repetir[0] != -1 && $repetir[0] > $min_short ) $min_short = $repetir[0];
                        if($repetir[1] != -1 && $repetir[1] > $min_syl ) $min_syl = $repetir[1];
                    }
                    if ($min_short != -1 || $min_syl != -1) {
                        echo "<form action='plantilles.php'method='post'>";
                        if( $min_short != -1 ) {
                            echo "</br> Hi ha Problemes de contingut a Short_Description, introdueix la plantilla per a Short_Description: </br><input type = 'text' name = 'short' pattern='.{". $min_short.",}' title='Cal introduir minim " . $min_short. " caracters' required> </br></br>";
                        }
                        if ($min_syl != -1) {
                            echo "</br> Hi ha Problemes de contingut a Syllabus, introdueix la plantilla per a Syllabus: </br><input type = 'text' name = 'syl' pattern='.{".$min_syl.",}' title='Cal introduir minim " . $min_syl. " caracters' required> </br></br>";
                        }
                        for($i = 0; $i < sizeof($_FILES['file']['name']); $i++){
                            $nom_new = $_POST['name'] . ($i + 1) . ".xml";
                            echo "<input type = 'hidden' name = 'new[]' value='$nom_new' >";
                        }
                        echo "<input type='submit' name='insert' value='Introduir plantilles'> </br>";
                    } else echo "</br> <INPUT TYPE='button' VALUE='Descarregar' onClick=\"location.href='download.php'\"></br>";
                    echo "</br><INPUT TYPE='button' VALUE='GetBack!' onClick=\"location.href='index.php'\"></br>";
                } else {
                    if(isset($_COOKIE['nou'])){
                        foreach(unserialize($_COOKIE['nou']) as $tit){
                            if (file_exists($tit)) unlink($tit);
                        }
                        setcookie("nou", "", time()-3600);
                    }
                ?>
                <form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
                    Introdueix el fitxer: </br><input type = "file" name = "file[]"  accept="text/xml" multiple="" required> </br></br>
                    Introdueix el nom del nou fitxer que sortirà: </br><input type = "text" name = "name" required> </br></br>
<input type="submit" name="boto"value="Submit"> </br>
</form>
</div>
<?php } ?>
</body> 
</html>
