<?php
$new = unserialize($_COOKIE['nou']);
$files = [];
foreach($new as $nom_new){
    if (file_exists($nom_new)){
        $files[] = $nom_new;
    }
}
setcookie("nou", "", time()-3600);
$zipname = "Resum" . str_replace(".xml", ".zip",$new[0]);
$zip = new ZipArchive;
$zip->open($zipname, ZipArchive::CREATE);
foreach ($files as $file) {
    $zip->addFile($file);
}
$zip->close();
foreach($files as $file){
    unlink($file);
}
if(filesize($zipname) == 0){
    echo "<script type='text/javascript'>alert('No s'ha trobat contingut o els cursos s'han esborrat per quelcom :S');</script>";
    //header("Refresh:0; url=index.php");
} else {
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename='.basename($zipname));
    header('Content-Length: ' . filesize($zipname));
    readfile($zipname);
    ob_clean();
    flush();
    unlink($zipname);
}
?>
