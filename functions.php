<?php
function correccio($data,$new){
    $xml = simplexml_load_file($data);
    $courses =[];
    $indices = [];
    $ins = [-1,-1];
    $mostra = "";
    $i = 0;
    foreach($xml->children() as $course){
        $isnew =  TRUE;
        //En aqui ens encarreguem de llegir el primer curs, ja que per defecte el saltem per comparar-ho els posteriors
        $isnew = comprova_date($course);
        if($isnew == false) $indices[] = $i;
        if (strlen((string)$course->SHORT_DESCRIPTION) < 100 && $isnew == TRUE){
            $min = strlen((string)$course->SHORT_DESCRIPTION);
            if($ins[0] == -1){
                echo "Mostra de Short_Description: " . (string)$course->SHORT_DESCRIPTION . "<br>";
                $ins[0] = $min;
            }
            if ($min < $ins[0]) $ins[0] = $min;
        }
        //Comencem a veure si hi ha SYLLABUS que no arriben al minim
        if (strlen((string)$course->SYLLABUS) < 400 && $isnew == TRUE){
            $min = strlen((string)$course->SYLLABUS);
            if($ins[1] == -1){
                echo "Mostra de Syllabus: " . (string)$course->SYLLABUS . "<br>";
                $ins[1] = $min;
            }
            if ($min < $ins[1]) $ins[1] = $min;
        }
        foreach($courses as $item){
            //A la primera passada, comprobem que no hi hagi grups repetits
            if((string)$course->TITLE == (string)$item->TITLE && (string)$course->METHOD_ID == (string)$item->METHOD_ID && (string)$course->NEXTS->NEXT->PLACE_ID  == (string)$course->NEXTS->NEXT->PLACE_ID ){
                $indices[]=$i;
                echo "Curs Repetit: " . (string)$course->TITLE. "<br>";
                $isnew = FALSE;
                break;
            }
        }
        if($isnew == TRUE){
            $courses[]= $course;
        }
        $i++;
    }
    foreach(array_reverse($indices) as $i){
        unset($xml->children()[$i]);
    }
    echo " Numero de cursos aprovats (DATE,No Repetits) de " . sizeof($courses). " : " . $i;
    if( $i == 0){
        echo "<script type='text/javascript'>alert('No hia cursos per descàrregar');</script>";
        header("Refresh:0; url=index.php");
    }
    $xml->asXML($new);
    unlink($data);
    if( $ins[1] != -1 ){
        $ins[1] = 400 - $ins[1];
    } else if ($ins[0] != -1) {
        $ins[0] = 100 - $ins[0];
    } return $ins;
}

function comprova_date($course){
    //Controlar dades y esborrar
    if((string)$course->NEXTS->NEXT->DATE ) {
        if(substr((string)$course->NEXTS->NEXT->DATE,-4) < date("Y") || (substr((string)$course->NEXTS->NEXT->DATE,-4) == date("Y") && (substr((string)$course->NEXTS->NEXT->DATE,-7,-6) == date("m")))){
            echo $course->TITLE . " Curs Caducat! <br><br>";
            return false;
        } else return true;
    } else return true;
}

function inserir($nou,$short,$syl){
    $xml = simplexml_load_file($nou);
    $courses =[];
    $indices = [];
    $ins = [-1,-1];
    $mostra = "";
    $i = 0;
    foreach($xml->children() as $course){
        $isnew =  TRUE;
        //Comencem a veure si hi ha SHORTS que no arriben al minim
        if (strlen((string)$course->SHORT_DESCRIPTION) < 100 ){
            $course->SHORT_DESCRIPTION = "<![CDATA[". str_replace("<![CDATA[","",(string)$course->SHORT_DESCRIPTION) . " " . $short . "]]>";
        }
        //Comencem a veure si hi ha SYLLABUS que no arriben al minim
        if (strlen((string)$course->SYLLABUS) < 400 ){
            $course->SYLLABUS = "<![CDATA[". str_replace("<![CDATA[","",(string)$course->SYLLABUS) . " " . $syl . "]]>";
        }
        $courses[]= $course;
    }
    foreach(array_reverse($indices) as $i){
        unset($xml->children()[$i]);
    }
    $xml->asXML($nou);
}
//$course->SYLLABUS = "<![CDATA[".(string)$course->SYLLABUS . "Successful completion of the course gives you a qualification that is accepted for entry into higher education for studies in a variety of fields.]] ";
//COMPROVAR DATA/PLACEif((string)$course->TITLE == (string)$item->TITLE && (string)$course->xpath("//NEXT/PLACE_ID")[$i] == (string)$item->xpath("//NEXT/PLACE_ID")[$cont])*/
?>


